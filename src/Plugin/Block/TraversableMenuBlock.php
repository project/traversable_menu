<?php

namespace Drupal\traversable_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\system\Entity\Menu;

/**
 * Provides a 'Traversable Menu' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "traversable_menu",
 *   admin_label = @Translation("Traversable Menu")
 * )
 */
class TraversableMenuBlock extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * This method sets the block default configuration. This configuration
   * determines the block's behavior when a block is initially placed in a
   * region. Default values for the block configuration form should be added to
   * the configuration array. System default configurations are assembled in
   * BlockBase::__construct() e.g. cache setting and block title visibility.
   *
   * @see \Drupal\block\BlockBase::__construct()
   */
  public function defaultConfiguration() {
    return [
      'block_traversable_menu' => 'admin',
      'block_traversable_menu_title' => 'Main Menu',
    ];
  }

  /**
   * {@inheritdoc}
   *
   * This method defines form elements for custom block configuration. Standard
   * block configuration fields are added by BlockBase::buildConfigurationForm()
   * (block title and title visibility) and BlockFormController::form() (block
   * visibility settings).
   *
   * @see \Drupal\block\BlockBase::buildConfigurationForm()
   * @see \Drupal\block\BlockFormController::form()
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menus = Menu::loadMultiple();
    $options = [];
    foreach ($menus as $key => $value) {
      $options[$key] = $value->label();
    }
    $form['block_traversable_menu'] = [
      '#type' => 'select',
      '#title' => $this->t('Menu'),
      '#options' => $options,
      '#description' => $this->t('Select the menu source.'),
      '#default_value' => $this->configuration['block_traversable_menu'],
    ];
    $form['block_traversable_menu_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Root level panel title'),
      '#description' => $this->t('This text will appear under the root panel of the menu.'),
      '#default_value' => $this->configuration['block_traversable_menu_title'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * This method processes the blockForm() form fields when the block
   * configuration form is submitted.
   *
   * The blockValidate() method can be used to validate the form submission.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['block_traversable_menu']
      = $form_state->getValue('block_traversable_menu');
    $this->configuration['block_traversable_menu_title']
      = $form_state->getValue('block_traversable_menu_title');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_tree_service = \Drupal::service('menu.link_tree');
    $menu_parameters = new MenuTreeParameters();
    $tree = $menu_tree_service->load($this->configuration['block_traversable_menu'], $menu_parameters);
    $build = $menu_tree_service->build($tree);
    $build['#theme'] = 'menu__traversable_menu';
    $build['#attached']['library'] = ['traversable_menu/traversable_menu'];
    $build['#attached']['drupalSettings']['traversable_menu']['block_traversable_menu_title'] = $this->configuration['block_traversable_menu_title'];
    return $build;
  }

}
