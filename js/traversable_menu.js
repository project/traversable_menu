document.addEventListener("DOMContentLoaded", function () {
  var traversable = new TraversableMenu(
    {
      'panel_title_first': drupalSettings.traversable_menu.block_traversable_menu_title
        // See options in bottom of traversable_menu.js to change selectors and other options
    }
  );
});
