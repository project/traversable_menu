INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the webform module to the modules directory of your Drupal
   installation.

 2. Enable the 'Traversable Menu' module. (/admin/modules)
